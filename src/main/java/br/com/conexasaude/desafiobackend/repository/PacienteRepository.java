package br.com.conexasaude.desafiobackend.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conexasaude.desafiobackend.model.Paciente;

public interface PacienteRepository extends JpaRepository<Paciente, Long> {

	Optional<Paciente> findByCpf(String cpf);

	List<Paciente> findAllByAtivo(Boolean ativo);
}
