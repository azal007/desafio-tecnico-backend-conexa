package br.com.conexasaude.desafiobackend.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.conexasaude.desafiobackend.model.Atendimento;

public interface AtendimentoRepository extends JpaRepository<Atendimento, Long> {

	@Query("FROM Atendimento WHERE medico.id = ?1 AND dataHoraAtendimento > ?2")
	List<Atendimento> listarAtendimentosAposData(Long id, LocalDateTime dataHora);

}
