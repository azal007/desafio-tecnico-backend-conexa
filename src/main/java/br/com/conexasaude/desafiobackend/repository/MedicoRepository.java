package br.com.conexasaude.desafiobackend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conexasaude.desafiobackend.model.Medico;

public interface MedicoRepository extends JpaRepository<Medico, Long> {

	Optional<Medico> findByEmail(String email);

}
