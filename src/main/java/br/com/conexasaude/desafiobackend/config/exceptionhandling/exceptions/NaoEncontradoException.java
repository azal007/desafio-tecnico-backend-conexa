package br.com.conexasaude.desafiobackend.config.exceptionhandling.exceptions;

public class NaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NaoEncontradoException() {
	}
}
