package br.com.conexasaude.desafiobackend.config.exceptionhandling.exceptions;

public class ErroNegocioException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ErroNegocioException(String message) {
		super(message);
	}	
}
