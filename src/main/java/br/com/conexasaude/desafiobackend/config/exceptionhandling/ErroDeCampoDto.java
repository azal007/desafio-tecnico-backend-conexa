package br.com.conexasaude.desafiobackend.config.exceptionhandling;

public class ErroDeCampoDto {
	
	private String campo;
	private String erro;
	
	public ErroDeCampoDto(String campo, String erro) {
		this.campo = campo;
		this.erro = erro;
	}

	public String getCampo() {
		return campo;
	}

	public String getErro() {
		return erro;
	}
	
	

}
