package br.com.conexasaude.desafiobackend.config.exceptionhandling;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class RetornoJsonDto {

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime dataHora;
	private String mensagem;
	
	public RetornoJsonDto(String mensagem) {
		super();
		this.mensagem = mensagem;
		this.dataHora = LocalDateTime.now();
	}	
}
