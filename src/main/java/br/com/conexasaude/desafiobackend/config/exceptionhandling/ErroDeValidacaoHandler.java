package br.com.conexasaude.desafiobackend.config.exceptionhandling;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.conexasaude.desafiobackend.config.exceptionhandling.exceptions.ErroNegocioException;
import br.com.conexasaude.desafiobackend.config.exceptionhandling.exceptions.NaoEncontradoException;

@RestControllerAdvice
public class ErroDeValidacaoHandler {
	
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Erros de validação das constraints existentes nas classes Form
	 * @param exception Exceção
	 * @return Lista dos erros conforme idioma do locale da requisição
	 */
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public List<ErroDeCampoDto> handle(MethodArgumentNotValidException exception) {
		List<ErroDeCampoDto> listaErros = new ArrayList<>();
		
		List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
		fieldErrors.forEach(e -> {
			String mensagem = messageSource.getMessage(e, LocaleContextHolder.getLocale());
			ErroDeCampoDto erro = new ErroDeCampoDto(e.getField(), mensagem);
			listaErros.add(erro);
		});		
		return listaErros;
	}

	/**
	 * Erros lançados manualmente devido a regras do negócio
	 * @param exception
	 * @return Mensagem de erro e data/hora atual
	 */
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ErroNegocioException.class)
	public RetornoJsonDto handle(ErroNegocioException exception) {
		return new RetornoJsonDto(exception.getMessage());
	}
	
	/**
	 * Erros lançados manualmente devido a regras do negócio
	 * @param exception
	 * @return Mensagem de erro e data/hora atual
	 */
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	@ExceptionHandler(NaoEncontradoException.class)
	public RetornoJsonDto handle(NaoEncontradoException exception) {
		return new RetornoJsonDto("Não foi encontrado nenhum registro com o ID informado.");
	}
}
