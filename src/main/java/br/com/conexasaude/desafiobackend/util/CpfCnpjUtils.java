package br.com.conexasaude.desafiobackend.util;

import java.text.DecimalFormat;

import org.apache.commons.lang3.StringUtils;

public class CpfCnpjUtils {
	
	public static String formatarCpf(String cpf) {
		if (cpf == null || "".equals(cpf.trim())) {
			return "";
		}
		String cpfComZerosAEsquerda = new DecimalFormat("00000000000").format(Long.parseLong(cpf));
		return cpfComZerosAEsquerda.substring(0, 3) + "." + cpfComZerosAEsquerda.substring(3, 6) + "." + cpfComZerosAEsquerda.substring(6, 9) + "-" + cpfComZerosAEsquerda.substring(9);
	}
	
	public static String formatarCnpj(String cnpj) {
		if (cnpj == null || "".equals(cnpj.trim())) {
			return "";
		}
		String cnpjComZerosAEsquerda = new DecimalFormat("00000000000000").format(Long.parseLong(cnpj));
		return cnpjComZerosAEsquerda.substring(0, 2) + "." + cnpjComZerosAEsquerda.substring(2, 5) + "." + cnpjComZerosAEsquerda.substring(5, 8) + "/" + cnpjComZerosAEsquerda.substring(8, 12) + "." + cnpjComZerosAEsquerda.substring(12);
	}
	
	public static boolean isCpfCnpjValido(String cpfCnpj) {
		if (cpfCnpj == null) {
			return true;
		} else if (cpfCnpj.length() <= 11) {
			return isCpfValido(cpfCnpj);
		} else {
			return isCnpjValido(cpfCnpj);
		}
	}

	public static boolean isCpfValido(String cpf) {
		try {
			cpf = StringUtils.leftPad(cpf, 11, '0');
			
			//Testa se o CPF é válido ou não
			int d1, d4, xx, nCount, resto, digito1, digito2;
			String check;
			String separadores = "/-.";
			d1 = 0;
			d4 = 0;
			xx = 1;
			for (nCount = 0; nCount < cpf.length() - 2; nCount++) {
				String saux = cpf.substring(nCount, nCount + 1);
				if (separadores.indexOf(saux) == -1) {
					d1 = d1 + (11 - xx) * Integer.valueOf(saux).intValue();
					d4 = d4 + (12 - xx) * Integer.valueOf(saux).intValue();
					xx++;
				}
			}
			resto = (d1 % 11);
			if (resto < 2) {
				digito1 = 0;
			} else {
				digito1 = 11 - resto;
			}

			d4 = d4 + 2 * digito1;
			resto = (d4 % 11);
			if (resto < 2) {
				digito2 = 0;
			} else {
				digito2 = 11 - resto;
			}

			check = String.valueOf(digito1) + String.valueOf(digito2);
			String saux2 = cpf.substring(cpf.length() - 2, cpf.length());

			if (saux2.compareTo(check) != 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	private static boolean isCnpjValido(String cnpj) {
		int soma = 0;
		cnpj = StringUtils.leftPad(cnpj, 14, '0');
		
		if (cnpj.length() == 14) {
			for (int i = 0, j = 5; i < 12; i++) {
				soma += j-- * (cnpj.charAt(i) - '0');
				if (j < 2) {
					j = 9;
				}
			}
			soma = 11 - (soma % 11);
			if (soma > 9) {
				soma = 0;
			}
			if (soma == (cnpj.charAt(12) - '0')) {
				soma = 0;
				for (int i = 0, j = 6; i < 13; i++) {
					soma += j-- * (cnpj.charAt(i) - '0');
					if (j < 2) {
						j = 9;
					}
				}
				soma = 11 - (soma % 11);
				if (soma > 9) {
					soma = 0;
				}
				if (soma == (cnpj.charAt(13) - '0')) {
					return true;
				}
			}
		}
		return false;
	}
}
