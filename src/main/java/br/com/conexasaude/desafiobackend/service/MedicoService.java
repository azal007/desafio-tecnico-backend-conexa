package br.com.conexasaude.desafiobackend.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.conexasaude.desafiobackend.config.exceptionhandling.exceptions.ErroNegocioException;
import br.com.conexasaude.desafiobackend.model.Medico;
import br.com.conexasaude.desafiobackend.repository.MedicoRepository;

@Service
public class MedicoService {

	@Autowired
	private MedicoRepository medicoRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Transactional
	public Medico incluir(Medico medico, String confirmacaoSenha) {
		if (!medico.getSenha().equals(confirmacaoSenha)) {
			throw new ErroNegocioException("A senha e a confirmação de senha devem ser idênticas!");
		}
		if (medicoRepository.findByEmail(medico.getEmail()).isPresent()) {
			throw new ErroNegocioException("E-mail já cadastrado!");
		}
		medico.setSenha(passwordEncoder.encode(medico.getSenha()));
		medicoRepository.save(medico);
		return medico;
	}
}
