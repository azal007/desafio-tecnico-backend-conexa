package br.com.conexasaude.desafiobackend.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data @EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Paciente {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;
	
	@NotNull
	private String nome;
	@NotNull @Column(unique = true)
	private String cpf;
	private Integer idade;
	private String email;
	private String telefone;
	@NotNull
	private Boolean ativo = true;
	@NotNull
	private LocalDateTime dataCadastro = LocalDateTime.now();
}
