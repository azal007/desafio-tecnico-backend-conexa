package br.com.conexasaude.desafiobackend.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data @EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TokenInvalidado {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;
	@NotNull @Column(unique = true)
	private String token;
	
	@NotNull
	private LocalDateTime dataCadastro = LocalDateTime.now();
	
	public TokenInvalidado(String token) {
		this.token = token;
	}
}
