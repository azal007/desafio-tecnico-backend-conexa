package br.com.conexasaude.desafiobackend.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data @EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Atendimento {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;
	
	@ManyToOne
	private Medico medico;
	@ManyToOne
	private Paciente paciente;
	@NotNull
	private LocalDateTime dataHoraAtendimento;
	@NotNull
	private LocalDateTime dataCadastro = LocalDateTime.now();
	
	@OneToMany(mappedBy = "atendimento", cascade = {CascadeType.ALL})
	private List<SintomaAtendimento> sintomas = new ArrayList<SintomaAtendimento>();
	
}
