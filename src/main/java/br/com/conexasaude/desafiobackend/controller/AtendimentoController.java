package br.com.conexasaude.desafiobackend.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conexasaude.desafiobackend.controller.dto.AtendimentoDto;
import br.com.conexasaude.desafiobackend.controller.form.AtendimentoForm;
import br.com.conexasaude.desafiobackend.model.Atendimento;
import br.com.conexasaude.desafiobackend.service.AtendimentoService;

@RestController
@RequestMapping("/api/v1/attendance")
public class AtendimentoController {

	@Autowired
	private AtendimentoService atendimentoService;
	
	@PostMapping
	public ResponseEntity<AtendimentoDto> incluir(@RequestBody @Valid AtendimentoForm form, UriComponentsBuilder uriBuilder) {
		Atendimento atendimento = atendimentoService.incluir(form.converter());
		
		URI uri = uriBuilder.path("/atendimentos/{id}").buildAndExpand(atendimento.getId()).toUri();
		return ResponseEntity.created(uri).body(new AtendimentoDto(atendimento));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<AtendimentoDto> excluir(@PathVariable Long id) {
		atendimentoService.excluir(id);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping
	public List<AtendimentoDto> listar() {
		List<Atendimento> atendimentos = atendimentoService.listarFuturos();
		return atendimentos.stream().map(AtendimentoDto::new).collect(Collectors.toList());
	}
}
