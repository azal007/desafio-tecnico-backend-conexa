package br.com.conexasaude.desafiobackend.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data @AllArgsConstructor
public class SintomaAtendimentoDto {

	private String descricao;
	private String detalhes;	
}
