package br.com.conexasaude.desafiobackend.controller.dto;

import org.springframework.beans.BeanUtils;

import br.com.conexasaude.desafiobackend.model.Medico;
import lombok.Data;

@Data
public class MedicoDto {

	private Long id;
	private String email;
	private String specialidade;
	private String cpf;
	private Integer idade;
	private String telefone;
	
	public MedicoDto() {
	}
	
	public MedicoDto(Medico medico) {
		BeanUtils.copyProperties(medico, this);
	}
}
