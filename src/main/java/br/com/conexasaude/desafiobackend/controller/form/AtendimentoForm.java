package br.com.conexasaude.desafiobackend.controller.form;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.conexasaude.desafiobackend.model.Atendimento;
import br.com.conexasaude.desafiobackend.model.Paciente;
import br.com.conexasaude.desafiobackend.model.SintomaAtendimento;
import lombok.Data;

@Data
public class AtendimentoForm {

	@NotNull @Future @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime dataHora;
	@NotNull
	private Long idPaciente;
	@Valid
	private List<SintomaAtendimentoForm> sintomas = new ArrayList<SintomaAtendimentoForm>();
	
	public Atendimento converter() {
        Atendimento atendimento = new Atendimento();
		atendimento.setDataHoraAtendimento(dataHora);
		atendimento.setPaciente(new Paciente());
		atendimento.getPaciente().setId(idPaciente);
		atendimento.setSintomas(sintomas
				.stream()
				.map(sf -> new SintomaAtendimento(atendimento, sf.getDescricao(), sf.getDetalhes()))
				.collect(Collectors.toList()));
		
        return atendimento;
    }
}
