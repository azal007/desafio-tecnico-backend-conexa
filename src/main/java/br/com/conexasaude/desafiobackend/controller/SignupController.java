package br.com.conexasaude.desafiobackend.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conexasaude.desafiobackend.controller.dto.MedicoDto;
import br.com.conexasaude.desafiobackend.controller.form.MedicoForm;
import br.com.conexasaude.desafiobackend.model.Medico;
import br.com.conexasaude.desafiobackend.service.MedicoService;

@RestController
public class SignupController {

	@Autowired
	private MedicoService medicoService;
	
	@PostMapping("/api/v1/signup")
	public ResponseEntity<?> incluir(@RequestBody @Valid MedicoForm form, UriComponentsBuilder uriBuilder) {
		Medico medico = medicoService.incluir(form.converter(), form.getConfirmacaoSenha());
		
		URI uri = uriBuilder.path("/medicos/{id}").buildAndExpand(medico.getId()).toUri();
		return ResponseEntity.created(uri).body(new MedicoDto(medico));
	}
}
