package br.com.conexasaude.desafiobackend.repository;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.conexasaude.desafiobackend.model.Medico;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MedicoRepositoryTest {

	@Autowired
	private MedicoRepository repository;
	
	@Autowired
	private TestEntityManager em;
	
	@Before
	public void init() {
		Medico medico = new Medico();
		medico.setEmail("medicocadastrado@gmail.com");
		medico.setSenha("senha");
		medico.setCpf("833.189.140-61");
		medico.setIdade(50);
		medico.setSpecialidade("Cardiologista");
		medico.setTelefone("(21)2244-7613");
		em.persist(medico);
	}
	
	@Test
	public void deveriaCarregarUmMedicoAoBuscarPeloSeuEmail() {
		String email = "medicocadastrado@gmail.com";
		Optional<Medico> optionalMedico = repository.findByEmail(email);
		Assert.assertNotNull(optionalMedico);
		Assert.assertFalse(optionalMedico.isEmpty());
		Assert.assertEquals(email, optionalMedico.get().getEmail());		
	}

	@Test
	public void naoDeveriaCarregarUmMedicoCujoEmailNaoEstejaCadastrado() {
		String email = "naocadastrado@gmail.com";
		Optional<Medico> optionalMedico = repository.findByEmail(email);
		Assert.assertNotNull(optionalMedico);
		Assert.assertTrue(optionalMedico.isEmpty());
	}
	
}
