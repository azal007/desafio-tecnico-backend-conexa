package br.com.conexasaude.desafiobackend.controller;

import java.net.URI;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class AutenticacaoControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void aoLogardeveriaDevolver400CasoEmailNaoEstejaCadastrado() throws Exception {
		URI uri = new URI("/api/v1/login");
		
		JSONObject json = new JSONObject();
		json.put("email", "naocadastrado@gmail.com");
		json.put("senha", "1234");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isBadRequest());		
	}

	@Test
	public void aoLogardeveriaDevolver400CasoSenhaEstejaErrada() throws Exception {
		URI uri = new URI("/api/v1/login");

		JSONObject json = new JSONObject();
		json.put("email", "emailautenticacao@gmail.com");
		json.put("senha", "errada");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isBadRequest());		
	}

	@Test
	public void aoLogardeveriaDevolver200CasoDadosDeAutenticacaoEstejamValidos() throws Exception {
		String email = "login200@email.com";
		signup(email);
		
		URI uri = new URI("/api/v1/login");
		
		JSONObject jsonLogin = new JSONObject();
		jsonLogin.put("email", email);
		jsonLogin.put("senha", "1234");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(jsonLogin.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk());		
	}
	
	@Test
	public void aoDeslogarDeveriaDevolver200CasoHajaSucesso() throws Exception {
		String email = "logoff200@email.com";
		signup(email);
		
		URI uri = new URI("/api/v1/login");
		
		JSONObject jsonLogin = new JSONObject();
		jsonLogin.put("email", email);
		jsonLogin.put("senha", "1234");
		
		String bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(jsonLogin.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

		JSONObject jsonRespostaLogin = new JSONObject(bodyResposta);

		uri = new URI("/api/v1/logoff");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.header("Authorization", "Bearer " + jsonRespostaLogin.get("token"))
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk());		
	}

	@Test
	public void aoDeslogarDeveriaDevolver403CasoTokenSejaInvalido() throws Exception {
		URI uri = new URI("/api/v1/logoff");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.header("Authorization", "Bearer " + "tokeninvalido")
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isForbidden());		
	}
	
	@Test
	public void aoDeslogarDeveriaDevolver403CasoTokenNaoSejaInformado() throws Exception {
		URI uri = new URI("/api/v1/logoff");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isForbidden());		
	}

	private void signup(String email) throws Exception {
		URI uri = new URI("/api/v1/signup");
		
		JSONObject jsonSignup = new JSONObject();
		jsonSignup.put("email", email);
		jsonSignup.put("senha", "1234");
		jsonSignup.put("confirmacaoSenha", "1234");
		jsonSignup.put("specialidade", "Cardiologista");
		jsonSignup.put("cpf", "993.077.970-13");
		jsonSignup.put("idade", "33");
		jsonSignup.put("telefone", "(21) 3232-6565");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(jsonSignup.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());
	}
}
