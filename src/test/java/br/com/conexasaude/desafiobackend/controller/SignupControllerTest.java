package br.com.conexasaude.desafiobackend.controller;

import java.net.URI;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class SignupControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void deveriaDevolver201AoCadastrarMedicoComDadosCorretos() throws Exception {
		URI uri = new URI("/api/v1/signup");
		
		JSONObject json = new JSONObject();
		json.put("email", "medicodadoscorretos@email.com");
		json.put("senha", "1234");
		json.put("confirmacaoSenha", "1234");
		json.put("specialidade", "Clínico Geral");
		json.put("cpf", "993.077.970-13");
		json.put("idade", "33");
		json.put("telefone", "(21) 3232-6565");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());		
	}
	
	@Test
	public void deveriaDevolver400SeSenhaDiferenteDeConfirmacaoSenha() throws Exception {
		URI uri = new URI("/api/v1/signup");
		
		JSONObject json = new JSONObject();
		json.put("email", "medicosenhadiferente@email.com");
		json.put("senha", "aaa");
		json.put("confirmacaoSenha", "bbb");
		json.put("specialidade", "Clínico Geral");
		json.put("cpf", "993.077.970-13");
		json.put("idade", "33");
		json.put("telefone", "(21) 3232-6565");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isBadRequest());		
	}
	
	@Test
	public void deveriaDevolver400SeEmailJaCadastrado() throws Exception {
		URI uri = new URI("/api/v1/signup");
		
		JSONObject json = new JSONObject();
		json.put("email", "cadastrado@email.com");
		json.put("senha", "aaa");
		json.put("confirmacaoSenha", "aaa");
		json.put("specialidade", "Cardiologista");
		json.put("cpf", "993.077.970-13");
		json.put("idade", "33");
		json.put("telefone", "(21) 3232-6565");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());
		
		json.put("email", "cadastrado@email.com");
		json.put("senha", "bbb");
		json.put("confirmacaoSenha", "bbb");
		json.put("specialidade", "Cirurgião");
		json.put("cpf", "350.815.780-61");
		json.put("idade", "44");
		json.put("telefone", "(21) 3232-6565");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void deveriaDevolver400SeEmailInvalido() throws Exception {
		URI uri = new URI("/api/v1/signup");
		
		JSONObject json = new JSONObject();
		json.put("email", "emailinvalido");
		json.put("senha", "aaa");
		json.put("confirmacaoSenha", "aaa");
		json.put("specialidade", "Cardiologista");
		json.put("cpf", "993.077.970-13");
		json.put("idade", "33");
		json.put("telefone", "(21) 3232-6565");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isBadRequest());		
	}
}
